package com.oreillyauto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.AddressBookRepository;
import com.oreillyauto.domain.Address;
import com.oreillyauto.service.AddressBookService;

@Service
public class AddressBookServiceImpl implements AddressBookService {
	@Autowired
	private AddressBookRepository addressRepo;

	@Override
	public List<Address> getAddresses() {
		return addressRepo.getAddresses();
	}

	@Override
	public Address saveAddress(Address address) {
		Address savedAddress = addressRepo.save(address);
		return savedAddress;
	}

	@Override
	public Address getAddressById(Integer id) {
		return addressRepo.getAddressById(id);
	}

	@Override
	public void deleteAddressById(Integer id) {
		addressRepo.deleteById(id);
	}
}
