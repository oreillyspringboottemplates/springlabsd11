package com.oreillyauto.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.UserRolesRepository;
import com.oreillyauto.domain.examples.UserRole;
import com.oreillyauto.service.UserRolesService;

@Service("userRolesService")
public class UserRolesServiceImpl implements UserRolesService {

	@Autowired
	UserRolesRepository userRepo;
	
	public UserRolesServiceImpl() {
	}

	@Override
	public List<UserRole> getUserRoles() {
		List<UserRole> finalList = new ArrayList<UserRole>();
		
		for (UserRole userRole : (List<UserRole>)userRepo.findAll()) {
			if (!listContainsRole(finalList, userRole.getRole())) {
				finalList.add(userRole);
			}
		}
		
		return finalList;
	}

	private boolean listContainsRole(List<UserRole> finalList, String role) {
		for (UserRole userRole : finalList) {
			if (userRole.getRole().equalsIgnoreCase(role)) {
				return true;
			}
		}
		
		return false;
	}

	
}
