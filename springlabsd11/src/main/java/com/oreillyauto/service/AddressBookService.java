package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Address;

public interface AddressBookService {
	public List<Address> getAddresses();
	public Address saveAddress(Address address);
	public Address getAddressById(Integer id);
	public void deleteAddressById(Integer id);
}
