package com.oreillyauto.controllers;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.Address;
import com.oreillyauto.dto.Message;
import com.oreillyauto.service.AddressBookService;
import com.oreillyauto.util.Helper;

@Controller
public class AddressBookController {
	
	@Autowired
	AddressBookService addressBookService;
	
	@GetMapping(value = "/addressBook")
	public String getAddressBook(Model model) {
		List<Address> addressList = addressBookService.getAddresses();
		for (Address address : addressList) {
			System.out.println(address);
		}
		return "addressBook";
	}
	
	@ResponseBody
	@GetMapping(value = "/addressBook/getAddresses")
	public List<Address> getAddressList() {
		try {
			return addressBookService.getAddresses();
		} catch(Exception e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}
	
	@ResponseBody
	@PostMapping(value = "/addressBook/save")
	public Message saveAddress(@RequestBody Address address) {
		Message message = new Message();
		try {
			addressBookService.saveAddress(address);
			message.setMessageType("success");
			message.setMessage("Successfully Saved!!!! :)");
			return message;
		} catch (Exception e) {
			message.setMessageType("danger");
			message.setMessage("Not cool.....");
			return message;
		}
	}
	
	@ResponseBody
	@GetMapping(value = "/addressBook/getAddressById")
	public Address getAddressById(Integer id) {
		try {
			return addressBookService.getAddressById(id);
		} catch(Exception e) {
			Address address = new Address();
			address.setMessage(e.getMessage());
			address.setMessageType("danger");
			return address;
		}
	}

	@ResponseBody
	@GetMapping(value = "/addressBook/delete")
	public Message deleteAddressById(Integer id) {
		Message message = new Message("success", "Record Successfully Deleted");
		
		try {
			addressBookService.deleteAddressById(id);
			return message;
		} catch(Exception e) {
			return new Message("danger", Helper.getErrorMessage(e));
		}
	}
	
//	@ResponseBody
//	@GetMapping(value = "/addressBook/delete")
//	public Address deleteAddressById(Integer id) {
//		try {
//			addressBookService.deleteAddressById(id);
//			Address address = new Address();
//			address.setMessage("Successfully Deleted");
//			address.setMessageType("success");
//			return address;
//		} catch(Exception e) {
//			Address address = new Address();
//			address.setMessage(e.getMessage());
//			address.setMessageType("danger");
//			return address;
//		}
//	}

}
