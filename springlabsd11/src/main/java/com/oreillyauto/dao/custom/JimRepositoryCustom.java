package com.oreillyauto.dao.custom;

import java.util.List;
import java.util.Map;

import com.oreillyauto.domain.Course;
import com.oreillyauto.domain.University;

public interface JimRepositoryCustom {    
    // Add your interface methods here (that ARE NOT CRUD or Spring Data methods [concrete implementations])
    public Map<String, Course> getDetails ();
    public Map<String, List<University>> getReport();
}
