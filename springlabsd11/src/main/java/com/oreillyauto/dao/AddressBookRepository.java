package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.AddressBookRepositoryCustom;
import com.oreillyauto.domain.Address;

public interface AddressBookRepository extends CrudRepository<Address, Integer>, AddressBookRepositoryCustom{

}
