package com.oreillyauto.domain.interns;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="INTERNS")
public class Intern implements Serializable {
	private static final long serialVersionUID = -5996834027120348470L;
    public Intern() {}
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "intern_id", columnDefinition = "INTEGER")
    private Integer internId;

    @Column(name = "intern_name", columnDefinition = "VARCHAR(64)")
	private String internName;

    @Transient
    private Integer issuer;
    
    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="intern")
    private Badge badge;
    
	public Integer getInternId() {
		return internId;
	}

	public void setInternId(Integer internId) {
		this.internId = internId;
	}

	public String getInternName() {
		return internName;
	}

	public void setInternName(String internName) {
		this.internName = internName;
	}

	public Badge getBadge() {
		return badge;
	}

	public void setBadge(Badge badge) {
		this.badge = badge;
	}

	public Integer getIssuer() {
		return issuer;
	}

	public void setIssuer(Integer issuer) {
		this.issuer = issuer;
	}

	@Override
	public String toString() {
		return "Intern [internId=" + internId + ", internName=" + internName + "]";
	}
    
}
