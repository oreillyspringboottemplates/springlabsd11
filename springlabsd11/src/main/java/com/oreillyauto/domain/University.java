package com.oreillyauto.domain;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the university database table.
 * 
 */
@Entity
@Table(name="university")
@NamedQuery(name="University.findAll", query="SELECT u FROM University u")
public class University implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="university_guid")
	private int universityGuid;

	@Column(name="university_name")
	private String universityName;

	//bi-directional many-to-one association to Course
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="course_id")
	private Course course;

	public University() {
	}

    public int getUniversityGuid() {
        return universityGuid;
    }

    public void setUniversityGuid(int universityGuid) {
        this.universityGuid = universityGuid;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "University [universityGuid=" + universityGuid + ", universityName=" + universityName + "]";
    }


}