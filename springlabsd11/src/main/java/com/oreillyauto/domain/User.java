package com.oreillyauto.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.oreillyauto.domain.examples.UserRole;
import com.oreillyauto.util.TimeDateHelper;

@Entity
@Table(name = "USERS")
public class User implements Serializable {

    private static final long serialVersionUID = 4803117695298165720L;

    public User() {
    }

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", columnDefinition = "INTEGER")
    private Integer userId;
	
	@Column(name = "first_name", columnDefinition = "VARCHAR(64)")
    private String firstName;
	
	@Column(name = "last_name", columnDefinition = "VARCHAR(256)")
    private String lastName;
	
	@Column(name = "department", columnDefinition = "VARCHAR(256)")
    private String department;
	
	@Column(name = "birth_date", columnDefinition = "TIMESTAMP")
    private Timestamp birthDate;

	@Transient
	private String action;
	
	@Transient
	private String birthDateFormatted;
	
	@Transient
	private String birthDateString;
	
	@Transient
	private Integer roleId;
	
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private List<UserRole> userroleList = new ArrayList<UserRole>();

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Timestamp getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Timestamp birthDate) {
		this.birthDate = birthDate;
	}

	public List<UserRole> getUserroleList() {
		return userroleList;
	}

	public void setUserroleList(List<UserRole> userroleList) {
		this.userroleList = userroleList;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getBirthDateFormatted() {
		String formatted = "";
		
		if (birthDate != null) {
			formatted = new SimpleDateFormat("MM/dd/yyyy").format(birthDate);	
		}
		
		return formatted;
	}

	public void setBirthDateFormatted(String birthDateFormatted) {
		this.birthDateFormatted = birthDateFormatted;
	}

	public String getBirthDateString() {
		return birthDateString;
	}

	public void setBirthDateString(String birthDateString) {
		this.birthDateString = birthDateString;
		
		try {
			birthDate = TimeDateHelper.convertOrlyDatepickerToTimestamp(birthDateString);
			System.out.println("timestamp set successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result + ((userroleList == null) ? 0 : userroleList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		if (userroleList == null) {
			if (other.userroleList != null)
				return false;
		} else if (!userroleList.equals(other.userroleList))
			return false;
		return true;
	}
	
	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", department="
				+ department + ", birthDate=" + birthDate + "]";
	}
}
