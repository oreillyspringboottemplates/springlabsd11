package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springlabsd11Application {

	public static void main(String[] args) {
		SpringApplication.run(Springlabsd11Application.class, args);
	}

}
